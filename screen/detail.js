import React from 'react';
import { ScrollView,Image, StyleSheet, Text, View } from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'

export default class DetailsScreen extends React.Component {
  render() {
    let pic = { uri: 'https://i.ytimg.com/vi/_SXdMYghPLw/hqdefault.jpg' }
    const { id, title, desc, picture } = 
           this.props.navigation.state.params;
    // console.log(this.props.navigation)
    return (
      <ScrollView>
        <View>
          <Image source={{uri:picture}}  style={{height:200}} />
          <Text>{title}</Text>
          <Text>{desc}</Text>
        </View>
      </ScrollView>
    );
  }
}


