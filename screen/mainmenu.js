import React from 'react';
import { FlatList, ScrollView, Image, StyleSheet, Text, View } from 'react-native';
// import HeaderBar from '../components/header';
import Card from '../components/card'
import ListView from '../components/listview'

import { TouchableOpacity } from 'react-native-gesture-handler';
import mydata from '../data/mydata';


export default class MainMenuScreen extends React.Component {
    static navigationOptions = {
        title: 'Main'
    };

    constructor(props) {
        super(props)
        this.state = { data: mydata, loading: false }
    }
    // https://mfumad12.firebaseio.com/mydata.json

    loadData = async () => {
        const res =
            await fetch('https://mfumad12.firebaseio.com/mydata.json')

        const netdata = await res.json()
        console.log(netdata)
        this.setState({ data: netdata })

    }

    async componentDidMount() {
        await this.loadData()
    }


    render() {
        let pic1 = { uri: 'https://i.ytimg.com/vi/_SXdMYghPLw/hqdefault.jpg' }
        // let pic2 = { uri: 'https://i.ytimg.com/vi/7xqwqD0c0Ls/hqdefault.jpg' }
        // let pic3 = { uri: 'https://i.ytimg.com/vi/T34PsWC-amM/hqdefault.jpg' }

        return (
            <ScrollView>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('OrderForm', item)}>

                            <ListView title={item.title}
                                desc={item.desc}
                                img={{ uri: item.picture }} />

                        </TouchableOpacity>
                    }
                />


                {/* <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Details')}>
          <Card img={pic1} title='ไข่เจียวฟู' />
        </TouchableOpacity>
        <Card img={pic2} />
        <Card img={pic3} />
        <Card />
        <Card /> */}
            </ScrollView>
        );
    }
}


