import React from 'react';
import { Linking, WebView, Share, FlatList, TextInput, Button, ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'
import QRCode from 'react-native-qrcode';
import Icon from 'react-native-vector-icons/FontAwesome';
import MIcon from 'react-native-vector-icons/MaterialIcons';

export default class OrderFormScreen extends React.Component {

    constructor(props) {
        super(props)
        const { id, title, desc, picture } =
            this.props.navigation.state.params;
        //   console.log("id=" + id)
        this.state = { data: [], pid: "" + id, ptitle: title }
    }

    loadData = async () => {
        const res =
            await fetch('https://mfumad12.firebaseio.com/myorder.json')

        const netdata = await res.json()
        console.log(netdata)

        const array = Object.values(netdata);
        console.log(array)
        this.setState({ data: array })

    }

    async componentDidMount() {
        await this.loadData()
    }


    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    'React Native | A framework for building native apps using React',
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    submitOrder = async () => {

        let myOrder = {
            id: this.state.pid,
            title: this.state.ptitle
        }
        let url = 'https://mfumad12.firebaseio.com/myorder.json'
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(myOrder),
        });
        await this.loadData()
    }

    render() {
        let pic = { uri: 'https://i.ytimg.com/vi/_SXdMYghPLw/hqdefault.jpg' }

        // console.log(this.props.navigation)
        return (
            <ScrollView>
                <View style={{ padding: 10 }}>
                    <Text style={{ fontSize: 30 }}>ID:</Text>
                    <TextInput
                        onChangeText={(id) => this.setState({ pid: id })}
                        style={{ fontSize: 30 }}
                        value={this.state.pid}></TextInput>

                    <Text style={{ fontSize: 30 }}>Title:</Text>
                    <TextInput
                        onChangeText={(title) => this.setState({ ptitle: title })}
                        style={{ fontSize: 30 }}
                        value={this.state.ptitle}></TextInput>

                    <Button
                        onPress={() => this.submitOrder()}
                        title='Make Order' />

                    <Button
                        onPress={() => this.props.navigation.navigate('QRCode')}
                        title='Scan QR-CODE' />

                    <Button onPress={this.onShare} title="Share" />

                    <Button title="Open Web" onPress={
                        () => { Linking.openURL('http://www.google.com') }} />

                    <Button title="Make Call" onPress={
                        () => { Linking.openURL('tel:+6622331223') }} />

                    <Button title="Mail To" onPress={
                        () => { Linking.openURL('mailto:vittayasak@mfu.ac.th') }} />

                    <Button title="Open Map" onPress={
                        () => { Linking.openURL('https://goo.gl/maps/DnbUSi1yh3q3fRLTA') }} />

                    <Icon name="rocket" size={200} color="#900" />
                    <MIcon name="delete" size={200} color="#900" />

                    <WebView
                        source={{ uri: 'https://www.youtube.com/embed/TcMBFSGVi1c' }}
                        style={{ marginTop: 20, height: 300 }}
                    />

                    {/* <QRCode
                        value={this.state.ptitle}
                        size={200}
                        bgColor='purple'
                        fgColor='white' />

                    <FlatList
                        data={this.state.data}
                        renderItem={({ item }) =>
                            <Text>{item.id} |  {item.title}</Text>
                        }
                    /> */}



                </View>
            </ScrollView>
        );
    }
}


